# Oracle 19 - Debezium POC

A simple POC to test and verify integrating [Debezium](https://debezium.io/) with an Oracle 19 database.
The integration uses LogMiner.

LogMiner enables the analysis of the contents of archived redo logs. It can be used to provide a historical view of the database without the need for point-in-time recovery. It can also be used to undo operations, allowing repair of logical corruption.


- Login into Oracle's Docker registry

This step is required to fetch the database image, since Oracle stuff is not publicly available.

```
docker login --username=<username> --password=<password> container-registry.oracle.com
```

- Start the database (Oracle Enterprise 19)

```
docker run -it --name oracledb19 -p 1521:1521 -e ORACLE_PWD=top_secret container-registry.oracle.com/database/enterprise:19.3.0.0
```

... this will take a while, grab a cup of ☕.

- Enable LogMiner in the database and create users

This steps configures the database to enable LogMiner.
Additionally, it creates a LogMiner Tablespace and User and a second user for the demo data.

```
docker exec -it oracledb19 bash

mkdir -p /opt/oracle/oradata/recovery_area

curl https://raw.githubusercontent.com/debezium/oracle-vagrant-box/main/setup-logminer.sh | sh

```

- Populate DB

Just create some data for demo purposes.

```
curl https://raw.githubusercontent.com/debezium/debezium-examples/main/tutorial/debezium-with-oracle-jdbc/init/inventory.sql | sqlplus debezium/dbz@//localhost:1521/ORCLPDB1
```

- Start Kafka, ZooKeeper and the Debezium Kafka connector

```
export DEBEZIUM_VERSION=1.9
docker compose -f docker-compose-oracle.yaml up --build -d
```

- Register the Debezium Oracle Connector

Change the IP address of `database.hostname` in the `register-oracle-logminer.json` to match your current setup.
Since the database is running outside Docker Compose, the IP address typically corresponds to the IP of the actual machine.

```
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @register-oracle-logminer.json
```

This command should return no error.

The Oracle (LogMiner) integration should now be completed.
In order to test the setup, insert or update an existing record in the `customers` table and consume the Debezium messages on the target Kafka topic.

Insert a record:

```
# Create a test change record
echo "INSERT INTO customers VALUES (NULL, 'John', 'Doe', 'john.doe@example.com');" | docker exec -i oracledb19 sqlplus debezium/dbz@//localhost:1521/ORCLPDB1
```

Start the consumer:

```
docker-compose -f docker-compose-oracle.yaml exec kafka /kafka/bin/kafka-console-consumer.sh \
    --bootstrap-server kafka:9092 \
    --from-beginning \
    --property print.key=true \
    --topic oracledb19.DEBEZIUM.CUSTOMERS
```

Alternatively, connect directly to the database to modify the records in the `customers` table:

```
docker exec -i oracledb19 sqlplus debezium/dbz@//localhost:1521/ORCLPDB1
```
